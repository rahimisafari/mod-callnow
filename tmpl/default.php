<?php
defined('_JEXEC') or die;
?>

<div class="call-now-button" style="top:<?php echo $options->bottontop ?>%">
	<div>
		<div class="call-now-mobile">
			<a href="tel:<?php echo $options->callnumber ?>"
				onclick="_gaq.push(['_trackEvent','call now module','Click/Touch','<?php echo $options->callnumber ?>',1]);"
				class="call-link" title="<?php echo $options->calllinktitle ?>">
				<p class="call-text"> <?php echo $options->calltextmobile; ?> </p>

				<div class="quick-alo-ph-circle"></div>
				<div class="quick-alo-ph-circle-fill"></div>
				<div class="quick-alo-ph-img-circle"></div>
			</a>
		</div>


		<div class="call-link-Desktop">
			<a data-toggle="modal" data-target="#exampleModal"
				onclick="_gaq.push(['_trackEvent','call now module','Click/Touch','<?php echo $options->callnumber ?>',1]);"
				class="call-link" title="<?php echo $options->calllinktitle ?>">
				<p class="call-text"> <?php echo $options->calltext; ?> </p>

				<div class="quick-alo-ph-circle"></div>
				<div class="quick-alo-ph-circle-fill"></div>
				<div class="quick-alo-ph-img-circle"></div>
			</a>
		</div>
	</div>
</div>

<?php if ($showSms ): ?>
<div class="sms-now-button" style="top:<?php echo $options->smstop ?>%">
	<div>
		<div class="sms-now-mobile">
			<a href="sms:<?php echo $options->smsnumber ?>?&body=<?php echo $options->smsbody ?>"
				onclick="_gaq.push(['_trackEvent','call now module','Click/Touch/SMS','<?php echo $options->smsnumber ?>',1]);"
				class="call-link" title="<?php echo $options->calllinktitle ?>">
				<p class="call-text"> <?php echo $options->calltext; ?> </p>
				<div class="sms-img-circle"></div>
			</a>
		</div>
	</div>
</div>
<?php endif; ?>


<style>
	.call-link {
		text-decoration: none;
	}

	.callNumber {
		font-size: 14pt;
		font-weight: bold;
	}

	@media screen and (max-width: 1980px) {
		.call-now-button {
			display: flex !important;
			/*background: #1a1919;*/
		}

		.sms-now-button {
			display: flex !important;
			/*background: #1a1919;*/
		}

		.quick-call-button {
			display: block !important;
		}
	}

	@media screen and (min-width:1024px) {
		.call-now-mobile {
			display: none !important;
		}

		.sms-now-mobile {
			display: none !important;
		}
	}

	@media screen and (min-width: px) {
		.call-now-button .call-text {
			display: none !important;
		}

		.sms-now-button .call-text {
			display: none !important;
		}

		/*.call-now-button{display:none;}*/

	}

	@media screen and (min-width: px) {
		.call-now-button .call-text {
			display: none !important;
		}

		.sms-now-button .call-text {
			display: none !important;
		}
	}

	.call-now-button {
		top: 70%;
	}

	.call-now-button {
		left: 5%;
	}

	.call-now-button {
		/*background: #1a1919;*/
	}

	.call-now-button div a .quick-alo-ph-img-circle {
		background-color: #0c3;
	}

	.call-now-button .call-text {
		color: #fff;
	}

/*-------------------sms------------------------*/
	.sms-now-button {
		top: 85%;
	}

	.sms-now-button {
		left: 5%;
	}

	.sms-now-button {
		/*background: #1a1919;*/
	}

	.sms-now-button .call-text {
		color: #fff;
	}

</style>